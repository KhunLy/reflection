﻿using System;
using System.Linq;
using System.Reflection;

namespace DemoReflexion
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Personne p = Create<Personne>();
            Voiture v = Create<Voiture>();

            //var props = typeof(Personne).GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            var props = typeof(Personne).GetProperties((BindingFlags)52);

            foreach (var prop in props)
            {
                Console.WriteLine(prop.Name);
            }

            Hello(p);
            // R=4
            // W=2
            // X=1
            // 000
            // 111
        }

        static T Create<T>()
        {
            ConstructorInfo[] ctors =  typeof(T).GetConstructors();
            ConstructorInfo ctor = ctors.First();
            ParameterInfo[] parameters = ctor.GetParameters();
            
            object[] p =  parameters.Select(p => {
                object result = null;
                if(p.ParameterType.IsClass)
                {
                    result = null;
                }
                else
                {
                    result = Activator.CreateInstance(p.ParameterType);
                }
                return result;
            }).ToArray();
            T instance = (T)ctor.Invoke(p);
            return instance;
        }

        static void Hello<T>(T instance)
        {
            MethodInfo m = typeof(T).GetMethod("SePresenter", BindingFlags.NonPublic | BindingFlags.Instance);
            if(m != null)
            {
                m.Invoke(instance, new object[0]);
            }
            else {
                Console.WriteLine("Mon objet ne sait pas se présenter");
            }
        }
    }

    class Voiture
    {

    }

    class Personne
    {
        private DateTime _birthDate { get; set; }

        public Personne(DateTime birthDate)
        {
            _birthDate = birthDate;
        }

        public string Nom { get; set; }
        public string Prenom { get; set; }

        private void SePresenter()
        {
            Console.WriteLine($"Bonjour, je m'appelle {Nom} {Prenom}");
        }

    }
}
